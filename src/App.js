import React from "react";
import "./App.css";
import Header from "./components/Header";
import Login from "./pages/Login";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Create from "./pages/Create";
import Edit from "./pages/Edit/Index";
import PrivateRoute from "./helper/PrivateRoute";
import { Route, Switch, Redirect } from "react-router-dom";

const routes = {
  login: "/login",
  register: "/register",
  home: "/home",
  create: "/create",
  edit: "/edit/:id",
};

function App() {
  return (
    <div>
      <Header />
      <div className="container">
        <Switch>
          <Redirect exact from="/" to={routes.login}></Redirect>
          <Route exact path={routes.login} component={Login}></Route>
          <Route exact path={routes.register} component={Register}></Route>
        </Switch>
        <Switch>
          <PrivateRoute
            exact
            path={routes.home}
            component={Home}
          ></PrivateRoute>
          <PrivateRoute
            exact
            path={routes.create}
            component={Create}
          ></PrivateRoute>
          <PrivateRoute
            exact
            path={routes.edit}
            component={Edit}
          ></PrivateRoute>
        </Switch>
      </div>
    </div>
  );
}

export default App;
