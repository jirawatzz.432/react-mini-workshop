import React, { useState } from "react";
import { login } from "../api/api";

export default function Login(props) {
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    let user = {
      username,
      password,
    };
    try {
      const result = await login(user);
      console.log(result);
      // props.history.push("/home");
    } catch (error) {}
  };
  return (
    <div>
      <div className="text-center">
        <img
          src={process.env.PUBLIC_URL + "/assets/images/logo192.png"}
          width="120"
        />
      </div>
      <div className="d-flex justify-content-center">
        <div className="col-md-6 ">
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input
                type="text"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                onChange={(e) => setusername(e.target.value)}
              />
              <small id="emailHelp" class="form-text text-muted">
                We'll never share your email with anyone else.
              </small>
            </div>
            <div className="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword1"
                onChange={(e) => setpassword(e.target.value)}
              />
            </div>
            <div className="form-group form-check">
              <input
                type="checkbox"
                className="form-check-input"
                id="exampleCheck1"
              />
              <label className="form-check-label" for="exampleCheck1">
                Check me out
              </label>
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
