import React from "react";
import CreateForm from "../../components/CreateForm";
import Back from "../../components/Back";

export default function index(props) {
  return (
    <div>
      <Back url="/home" history={props.history} />
      <CreateForm check="Create" />
    </div>
  );
}
