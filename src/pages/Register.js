import React, { useState } from "react";
import { register } from "../api/api";

export default function Register() {
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");

  const save = async (e) => {
    e.preventDefault();
    let user = {
      username,
      password,
    };
    try {
      const result = await register(user);
      console.log(result);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="text-center">
        <img
          src={process.env.PUBLIC_URL + "/assets/images/logo192.png"}
          width="120"
        />
      </div>
      <div className="d-flex justify-content-center">
        <div className="col-md-6 ">
          <form onSubmit={save}>
            <div className="form-group">
              <label for="exampleInputEmail1">Username</label>
              <input
                type="text"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                onChange={(e) => setusername(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword1"
                onChange={(e) => setpassword(e.target.value)}
              />
            </div>
            <button type="submit" className="btn btn-warning btn-block">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
