import React, { useEffect, useState } from "react";
import CreateForm from "../../components/CreateForm";
import Back from "../../components/Back";
import { getUserById } from "../../api/api";

export default function Index(props) {
  const [user, setUser] = useState();

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    try {
      const user = await getUserById(props.match.params.id);
      setUser(user.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <Back url="/home" history={props.history} />
      <h1>Edit User</h1>
      <hr />
      <CreateForm check="Edit" user={user} history={props.history} />
    </div>
  );
}
