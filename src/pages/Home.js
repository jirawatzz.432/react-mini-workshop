import React, { useEffect, useState } from "react";
import HomeTitle from "../components/HomeTitle";
import HomeTable from "../components/HomeTable";
import HomeCreateButton from "../components/HomeCreateButton";
import { getAllUsers, DeleteUser } from "../api/api";

export default function Home(props) {
  const [user, setUser] = useState([]);

  useEffect(() => {
    fetchUser();
  }, []);

  const fetchUser = async () => {
    try {
      const user = await getAllUsers();
      setUser(user.data.data);
    } catch (error) {}
  };

  const nextCreate = () => {
    props.history.push("/create");
  };

  const deleteUser = async (id) => {
    console.log(id);

    try {
      await DeleteUser(id);
      fetchUser();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <HomeCreateButton create={nextCreate} />
      <HomeTitle name="All user" />
      <hr />
      <HomeTable user={user} delete={deleteUser} />
    </div>
  );
}
