import React, { useState, useEffect } from "react";
import { createUser, EditUser } from "../api/api";

export default function CreateForm(props) {
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [salary, setSalary] = useState("");

  useEffect(() => {
    if (props.user) {
      setName(props.user.data.name);
      setAge(props.user.data.age);
      setSalary(props.user.data.salary);
    }
  }, [props.user]);

  const save = async (e) => {
    e.preventDefault();
    let user = {
      name,
      age,
      salary,
    };
    try {
      await createUser(user);
      props.history.push("/home");
    } catch (error) {
      console.log(error);
    }
  };

  const edit = async (e) => {
    e.preventDefault();
    let user = {
      name,
      age,
      salary,
    };
    try {
      await EditUser(props.user.data._id, user);
      props.history.push("/home");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="text-center">
        <img
          src={process.env.PUBLIC_URL + "/assets/images/logo192.png"}
          width="120"
        />
      </div>
      <div className="d-flex justify-content-center">
        <div className="col-md-6 ">
          <form onSubmit={props.check === "Edit" ? edit : save}>
            <div class="form-group">
              <label for="name">Name</label>
              <input
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                class="form-control"
                id="name"
                aria-describedby="emailHelp"
              />
            </div>
            <div class="form-group">
              <label for="age">Age</label>
              <input
                type="number"
                value={age}
                onChange={(e) => setAge(e.target.value)}
                class="form-control"
                id="age"
              />
            </div>
            <div class="form-group">
              <label for="salary">Salary</label>
              <input
                type="number"
                value={salary}
                onChange={(e) => setSalary(e.target.value)}
                class="form-control"
                id="salary"
              />
            </div>
            <button type="submit" class="btn btn-success btn-block">
              {props.check}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
