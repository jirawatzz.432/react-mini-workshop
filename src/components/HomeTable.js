import React from "react";
import { Link } from "react-router-dom";

export default function HomeTable(props) {
  const { user } = props;
  console.log('user', user)
  return (
    <div className=" mt-3">
      <table className="table">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Age</th>
            <th scope="col">Salary</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {user.map((user, index) => {
            return (
              <tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>{user.name}</td>
                <td>{user.age}</td>
                <td>{user.salary}</td>
                <td>
                  <Link to={`/edit/${user._id}`}>
                    <span style={{ color: "blue" }}>Edit</span>
                  </Link>{" "}
                  |{" "}
                  <span
                    onClick={() => props.delete(user._id)}
                    style={{ cursor: "pointer", color: "red" }}
                  >
                    Delete
                  </span>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
