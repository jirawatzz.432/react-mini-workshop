import React from "react";

export default function HomeTitle(props) {
  return (
    <div>
      <h1>{props.name}</h1>
    </div>
  );
}
