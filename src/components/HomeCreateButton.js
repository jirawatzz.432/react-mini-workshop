import React from "react";

export default function HomeCreateButton(props) {
  return (
    <div>
      <button
        className="btn btn-success btn-block"
        onClick={() => props.create()}
      >
        Create User
      </button>
    </div>
  );
}
