import axios from "axios";

const url = "http://203.154.59.14:3000/api/v1";

export const getAllUsers = () => {
  //    axios.get(url + "/users").then((res) => {
  //     return res.data;
  //   });
  return axios.get(url + "/users");
};

export const getUserById = (id) => axios.get(url + "/users/" + id);

export const createUser = (user) => axios.post(url + "/users", user);

export const EditUser = (id, user) => axios.put(url + "/users/" + id, user);

export const DeleteUser = (id) => axios.delete(url + "/users/" + id);

export const login = (user) => axios.post(url + "/users/", user);

export const register = (user) => axios.post(url + "/users/", user);
